# Blog post on binary cross-entropy

Here are the resources necessary to reproduce the results of [my m²hycon blog post on binary cross-entropy](https://m2hycon.com/m2b/blog/bm-binary-cross-entropy/).

If you have any difficulties, please open an issue in this repository.

If you would like to make a correction, please open an issue, or even better a merge request.

## Contents

- [`scipy_optimize.py`](scipy_optimize.py) is a script which uses `scipy.optimize()` to compute the parameters which optimize the BCE loss.
- [`keras_optimize.py`](keras_optimize.py) is a script which uses a single-neuron Keras model to compute the parameters which optimize the BCE loss.
- [`bce-probabilistic-interpretation.ipynb`](bce-probabilistic-interpretation.ipynb) is the Jupyter notebook which was used to perform all the computations and produce all the graphics.

### Dependencies

A [Docker image](#dockerized-execution) is available, which will guarantee that the correct dependencies are installed.

Alternatively, you may wish to install some subset of these packages in a local environment. I recommend using the [`mamba`](https://github.com/mamba-org/mamba) command in a local Conda environment.

It is likely that everything will be compatible with a wide range of versions. As of the time of development, the current versions are as follows:

- Python v3.9.5
- scipy v1.6.3 for `scipy_optimize.py`
- Tensorflow v2.4.1 for `keras_optimize.py`
- Plotly v5.0.0 for `bce_probabalistic-interpretation`
    - `ipywidgets` for Plotly's interactive FigureWidget.
    - `python-kaleido` for rendering static images.
- PyMC3 v3.11.2 for generating the sample sigmoid parameters.

## Dockerized execution

In case there are any difficulties with running a script or notebook, I have dockerized my environment in order to ensure reproducibility.

### Keras script to find optimal parameter values

We can run `keras_optimize.py` in a Tensorflow Docker container as follows.

On Linux, Mac, or Windows Powershell, change into the directory of this repository, and then run

```bash
docker run --rm -it -v $PWD:/work -w /work tensorflow/tensorflow:2.4.1 python ./keras_optimize.py
```

This will cause a Tensorflow Docker image to be downloaded. To reclaim the disk space, run

```bash
docker rmi tensorflow/tensorflow:2.4.1
```

### Dockerized JupyterLab

We can run `scipy_optimize.py` and `bce-probabilistic-interpretation.ipynb` from JupyterLab.

To start JupyterLab, change into the directory of this repository and run

```bash
docker-compose up
```

This will cause a [Docker Hub image](https://hub.docker.com/repository/docker/maresb/bm-binary-cross-entropy) to be downloaded (875 MB compressed) and started.

In case the image is no longer available on Docker Hub, edit `docker-compose.yaml` and uncomment the line containing `build: .`. Then the image will be built locally. (This is slower than downloading.)

The following commands will reclaim the disk space when you are finished.

```bash
# Remove container and network
docker-compose down

# Remove downloaded image (or built layers in case of build)
docker rmi maresb/bm-binary-cross-entropy

# In case of build, remove the base image
docker rmi jupyter/base-notebook:5211732116f7
```
