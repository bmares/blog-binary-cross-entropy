---
title: A probabilistic interpretation of binary cross-entropy
author: bm
date: 2021-06-25
publisheddate: 2021-06-25
tags: ["binary cross-entropy", "BCE", "log loss", "logistic loss", "log probability", "maximum-likelihood estimation", "MLE", "probability", "Keras", "SciPy", "optimization", "classification", "Bayesian", "Jupyter", "loss function", "prediction", "model", "machine learning", "parameter", "math", "for mathematicians", "sigmoid", "MSE", "mean-squared error", "choice of loss function", "space cow"]
type: "post"
comments: false
katex: true
markup: "mmark"
draft: false
summary: >
  This post is about the most commonly used loss function for binary classification, known as "binary cross-entropy loss." My goal is to explain why this strange formula is optimal in a particular sense: it encodes the "likelihood" that a model will reproduce the observed data.
---

<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>

## Git repository

[A Jupyter notebook and Python code for this blog post](https://gitlab.com/bmares/blog-binary-cross-entropy) are available on GitLab.

## Introduction

This post is about the most commonly used loss function for binary classification:

$$
\mathcal{L} = \mathcal{L}_\mathrm{BCE} =
  -\sum_{n=1}^N
  \left[
  y_n \log \hat{y}_n + (1 - y_n) \log( 1 - \hat{y}_n )
  \right]. \label{BCE}\tag{BCE}
$$

This loss function is known variously as "binary cross-entropy loss," "cross-entropy loss," "log loss," or "logistic loss."

My goal is to explain why this strange formula is optimal in a particular sense: it encodes the "likelihood" that a model will reproduce the observed data. (I will *not* discuss the information-theoretic interpretation.)

**Note:** Often there is a normalization factor of $ 1/N $ so that $ \mathcal{L} $ represents an *average loss*; the expression we use here instead represents a *cumulative loss*. The choice of which one to use is simply a convention.

## Binary classification problems

Let's review the basics and explain notation.

In a binary classification problem, one is given an input of $ N $ *measurements* $ \left\\{ y_1,\ldots,y_N \right\\} $, each of which is either $ 0 $ (false) or $ 1 $ (true). (For example, $ y_n $ might represent whether or not there was rainfall on some day labeled by $ n $.)

Corresponding to each measurement $ y_n $, there is typically some *feature vector* $ x_n $ which provides the model with information about the measurement. (For example, $ x_n $ could be a tuple containing the average measured temperature, pressure, and humidity on the day $ n $, as well as the day number $ n $ itself.)

Given a feature vector $ x_n $, the model should produce a *prediction* $ \hat{y}_n $ which is some value in the interval $ \left[ 0, 1 \right] $. This value represents the model's best guess of the probability that $ y_n $ is true based on the information contained in the feature vector $ x_n $.

Since the prediction $ \hat{y}_n $ is only allowed to depend on the feature vector $ x_n $, it has the form of a function $ \hat{y}_n = f(x_n) $, where $ f(x) $ is some *model function* of particular form, such as a sigmoid or a neural net. For example, in logistic regression we use the sigmoid function $ f\_{w,b}(x) = \sigma(w x + b) $. Note that $ f(x) = f\_\theta (x) $ is allowed to depend on parameters $ \theta $, in this case the parameters $ \theta = (w, b)$ are the weight and bias. The process of searching for optimal values for model parameters is *training*.

In summary, our goal is to train a model to find the parameters $ \theta $ such that the predictions

$$ \hat{y}_n = f_{\theta}(x_n) \label{preds}\tag{preds}$$

are as accurate as possible. Namely, we want to find the parameters $ \theta $ which minimize $ \mathcal{L}(\theta) $ given by $ \eqref{BCE} $.

**Note**: How are we able to write $ \mathcal{L} $ as a function of $ \theta $ when $ \theta $ does not even appear in $ \eqref{BCE} $? It's because $ \hat{y}_n $ depends implicitly on $ \theta $ via the definition of $ \hat{y}_n $ in $ \eqref{preds} $. To be completely explicit,

$$
\begin{aligned}
 \mathcal{L} &= \mathcal{L}(y_1, \hat{y}_1) + \cdots + \mathcal{L}(y_N, \hat{y}_N) \\
 &= \mathcal{L}(y_1, f_{\theta}(x_1)) + \cdots + \mathcal{L}(y_N, f_{\theta}(x_N)). \label{params}\tag{params}\\
\end{aligned}
$$

Once we fix the data $ x_n $, $ y_n $, and also fix the model $ f $, the only remaining free parameters in the right-hand side of $ \eqref{params} $ are the model parameters $ \theta $. Thus once the data and model are fixed, $ \mathcal{L} = \mathcal{L}( \theta ) $.

### Warm-up example: no features

Let's first consider the case where there are no features.

Suppose that on 10 subsequent days we measure whether or not there was rainfall. The data may look something like this:

{.table .text-center .table-hover .w-25}
|  $n$  | $y_n$ |
|-------|-------|
|  $1$  |  $0$  |
|  $2$  |  $0$  |
|  $3$  |  $1$  |
|  $4$  |  $0$  |
|  $5$  |  $1$  |
|  $6$  |  $1$  |
|  $7$  |  $1$  |
|  $8$  |  $1$  |
|  $9$  |  $1$  |
| $10$  |  $1$  |

However, we will not tell the model which day is which. Effectively, we pose the following question to the model:

> We measured seven days with rainfall and three days without. What is your best guess for the probability of rainfall?

We can pose this question to the model by not putting any information into the feature vector. In other words, we set each feature vector $ x_n $ to be the empty vector $ () $ of length zero.

Since the input $ x_n $ is empty, for fixed data and parameters, the model can only predict some constant probability $ p $ independent of $ n $. (By $ \eqref{preds} $, the predictions $ \hat{y}\_n $ are only allowed to depend on $ n $ through $ x_n $, which is always the same zero-length vector.) Thus the only possible model parameter is the output probability $ p $.

For a given data set, which value of $ p $ will the model predict? After training, the model should predict the value of $ p $ which minimizes $ \mathcal{L}(p) $.

The binary cross-entropy loss function takes a simple form in the current case. There are seven instances of the label $ 1 $ and three instances of the label $ 0 $. Substituting $ \hat{y}_n = p $ and $ y_n = 0 $ or $ 1 $ according to the data into $ \eqref{BCE} $,

$$
\begin{aligned}
\mathcal{L}_\mathrm{BCE}(p) =& - \left( 1 \log p + 0 \log(1 - p) \right) \\
 & - \left( 0 \log p + 1 \log(1 - p) \right) \\
 & - \left( 1 \log p + 0 \log(1 - p) \right) \\
 & \quad\vdots \\
= & -\left(7 \log p + 3 \log(1 - p) \right) .
\end{aligned}
$$

Here is what the loss function $ \mathcal{L}_\mathrm{BCE}(p) $ looks like:

{{< plotly json="plots/bce_7_3.json" height="400px" >}}

It is easily verified (numerically and analytically) that loss is minimized when $p=0.7$, corresponding to 70% of the labels being $1$. This is a good sanity check: **without features, binary cross-entropy is minimized when the prediction matches the observed frequency of the data**.

This is not however a distinguishing characteristic of binary cross-entropy. For example, mean squared error loss has the same property.

Before explaining what makes binary cross-entropy special, let's consider a more interesting example.

### Example: logistic regression

Suppose we include the day in the feature vector. Thus $ x_n $ is the one-component vector $ (n) $:

{.table .text-center .table-hover .w-50}
|  $n$  | $x_n$ | $y_n$ |
|-------|-------|-------|
|  $1$  | $(1)$ |  $0$  |
|  $2$  | $(2)$ |  $1$  |
|  $3$  | $(3)$ |  $0$  |
|  $4$  | $(4)$ |  $1$  |
|  $5$  | $(5)$ |  $1$  |
|  $6$  | $(6)$ |  $0$  |
|  $7$  | $(7)$ |  $1$  |
|  $8$  | $(8)$ |  $1$  |
|  $9$  | $(9)$ |  $1$  |
| $10$  |$(10)$ |  $1$  |

The goal of logistic regression is to fit the best sigmoid

$$ \hat{y}=f_{w,b}(x_n)=\sigma(w n + b) $$

to this data.

<a id="sigmoids"></a>
{{< plotly json="plots/best_sigmoids.json" height="400px" >}}

The notion of "best" is subjective until one specifies a specific loss function. Above we plot the best sigmoids with respect to Binary Cross-Entropy loss and Mean Squared Error (MSE) loss.

Finding the optimal parameters $w$ and $b$ is done as in the previous example by minimizing the loss function. This time there are two parameters:

{{< plotly json="plots/bce_logistic.json" height="400px" >}}

The minimum is located at

$$
w_{\mathrm{opt}} = 1.26746, \quad b_{\mathrm{opt}} = -4.41512, \label{BCE-opt}\tag{BCE-opt}
$$

and can be easily determined with a short [SciPy script](scipy_optimize.py) or a single-neuron [Keras model](keras_optimize.py).

## A probabilistic approach

We now take a seemingly unrelated probabilistic approach. It is driven by the following question.

> How likely is the observed data to have resulted from a given choice of parameters?

We can perform an experiment where we use the probabilities from a given sigmoid curve to generate random binary data. We then compare the resulting random data with the observed data, and measure the frequency of an exact match.

This procedure suggests the following optimization problem:

> Find the parameters which maximize the likelihood of an exact match to the observed data.

This procedure is known as *Maximum Likelihood Estimation* (MLE).

In our example of rainfall on subsequent days, if we use the optimal BCE sigmoid with parameters $ \eqref{BCE-opt} $, then the probability of a perfect prediction is 8.243%. In comparison, the probability of a perfect prediction with the optimal MSE sigmoid is slightly lower at 7.357%.

Instead of having to rely on slow statistical simulations, it is easy and advantageous to compute these probabilities analytically.

For example, suppose we have a sigmoid which predicts $ \hat{y}_3 = \tfrac{1}{4} $ and $ \hat{y}_7 = \tfrac{5}{6} $. What is the probability of correctly predicting both $ y_3 = 0 $ and $ y_7 = 1 $? It is

$$
P(y_3 \textrm{ and } y_7 \textrm{ correct}) = \tfrac{5}{8} = \tfrac{3}{4} \times \tfrac{5}{6} = (1 - \hat{y}_3) \times \hat{y}_7.
$$

The factor corresponding to correctly predicting $ y_n $ is:

$$
P(y_n \textrm{ correct}) =
\begin{cases}
(1 - \hat{y}_n) & \textrm{if } y_n=0, \\
\hat{y}_n & \textrm{if } y_n=1.
\end{cases}
$$

Based on the usual rule $ p^0=1 $, we can express both cases with the single expression

$$
P(y_n \textrm{ correct}) = \hat{y}_n^{y_n} (1 - \hat{y}_n)^{1 - y_n}.
$$

The probability that all predictions are correct is then the product

$$
P(\textrm{all correct}) = \prod_{n=1}^N \hat{y}_n^{y_n} (1 - \hat{y}_n)^{1 - y_n}. \label{P-correct}\tag{P-correct}
$$

This expression is easy to graph or program into an optimizer. A familiar picture emerges if we plot the probability as a function of the parameters.

{{< plotly json="plots/prob_all_correct.json" height="400px" >}}

Indeed, the maximum likelihood again occurs at exactly $ \eqref{BCE-opt} $.

## Equivalence of both approaches

We have observed that minimizing binary cross-entropy seems to be equivalent to MLE, and we seek an explanation.

Probabilities are multiplicative. The probability of all observations being correct is a product over the individual observations. In contrast, the paradigm of a "loss function" is additive. We sum the losses over the individual observations.

The relation is solved by two common general principles:

* How does one convert between multiplication and addition?
  * Answer: logarithms and exponentiation.
* How does one convert between a maximization problem and a minimization problem?
  * Answer: multiply by minus one.

Minimizing $ \mathcal{L} $ is equivalent to maximizing $ -\mathcal{L} $. Since $ \exp $ is a strictly increasing function, maximizing $ -\mathcal{L} $ is equivalent to maximizing $ \exp( -\mathcal{L} ) $, and

$$
\begin{aligned}
\exp(-\mathcal{L}_\mathrm{BCE}) & =\exp\sum_{n=1}^{N}\left[y_{n}\log\hat{y}_{n}+(1-y_{n})\log(1-\hat{y}_{n})\right] \\
& =\prod_{n=1}^{N}\exp\left[y_{n}\log\hat{y}_{n}+(1-y_{n})\log(1-\hat{y}_{n})\right] \\
& =\prod_{n=1}^{N}\exp\left[y_{n}\log\hat{y}_{n}\right]\exp\left[(1-y_{n})\log(1-\hat{y}_{n})\right] \\
& =\prod_{n=1}^{N}\hat{y}_{n}^{y_{n}} (1-\hat{y}_{n})^{1-y_{n}} \\
& = P(\textrm{all correct}).
\end{aligned}
$$

Thus we have the following equivalence of binary cross-entropy and likelihood:

$$
\begin{aligned}
\exp(-\mathcal{L}_\mathrm{BCE}) & = P(\textrm{all correct}), \\
\mathcal{L}_\mathrm{BCE} & = -\log(P(\textrm{all correct})).
\end{aligned}
$$

This proves that maximum likelihood estimation and binary cross-entropy minimization are equivalent.

**Note**: The *average* BCE loss (with the factor of $ 1 / N $) encodes the [geometric mean](https://en.wikipedia.org/wiki/Geometric_mean) probability of a correct prediction:

$$
\exp(-\mathcal{L}_\mathrm{BCE} / N) = \sqrt[N]{\prod_{n=1}^N P(y_n \textrm{ correct})}.
$$

## Advantages and disadvantages of binary cross-entropy

When you are choosing a loss function for binary classification, when should you choose mean-squared error (MSE) over binary cross-entropy (BCE)?

The answer is never. MSE is not meant for classification, and its properties make it unsuitable. For instance, BCE correctly assigns an infinite penalty for an absolutely confident prediction $ \hat{y}_n = 1 $ when the corresponding observation is $ y_n = 0 $. In contrast, the MSE penalty is merely 4× greater than if the prediction had been $ \hat{y} = \tfrac{1}{2} $.

The BCE loss function has a firm theoretical basis in probability theory, so most of the time it "just works," especially for large datasets.

BCE is better suited for computation than $ \eqref{P-correct} $, because sums are more numerically stable than products. The product of many small probabilities can quickly lead to an underflow, while BCE is more resilient.

For small or pathological datasets, maximum likelihood estimation with BCE might produce unreliable results. This is not really a fault of the loss function itself, but rather a problem with the paradigm that a model should always be tuned to the "best" parameter choice, even when there are large uncertainties in the parameters. In our example, since there are so few data points, there is a broad range of plausible sigmoids. MLE throws away all but the most plausible.

{{< plotly json="plots/sampled_sigmoids.json" height="420px" >}}

Bayesian modeling is a more advanced paradigm which elegantly accounts for parameter uncertainty. For more details, see this [example of Bayesian logistic regression](https://nbviewer.jupyter.org/github/CamDavidsonPilon/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers/blob/master/Chapter2_MorePyMC/Ch2_MorePyMC_PyMC3.ipynb#Example:-Challenger-Space-Shuttle-Disaster-).

## Putting this to all to good use

It's great for intuition to understand this correspondence between binary cross-entropy and the (minus-log-) probability of correctly predicting the observed data. The benefits are not only theoretical; as we shall soon see, we can build on these ideas for practical purposes.

<center><img src="images/space-cow.jpg" width=50% alt="space cow"></center>

In a subsequent blog post, we will develop these ideas towards understanding the effect of imbalanced data on model predictions when BCE is used as the loss function. We will see precisely how to correct for imbalances. We will also see how to extend everything from binary to multi-class classification problems.
