FROM jupyter/base-notebook:5211732116f7

COPY environment.lock.yaml /tmp

RUN : \
  && mamba env update --name base --file /tmp/environment.lock.yaml \
  && mamba clean --all -f -y \
;
