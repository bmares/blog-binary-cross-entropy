#!/usr/bin/env python3

import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential

x = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
y = np.array([0, 0, 1, 0, 1, 1, 1, 1, 1, 1])

# Ensure that the result is deterministic.
tf.random.set_seed(0)

# Build a Keras neural network consisting of a single neuron with sigmoid activation.
model = Sequential()
model.add(Dense(1, input_dim=1, activation="sigmoid"))

# Set loss to BCE and use Adam optimizer.
model.compile(
    loss=tf.keras.losses.BinaryCrossentropy(),
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.3),
)

# Perform gradient descent.
model.fit(x, y, epochs=400, verbose=0)

# Get tf.Variable objects corresponding to weight and bias.
w_var, b_var = model.layers[0].weights

# Extract the numerical values from the corresponding numpy arrays and print the result.
w_bce_opt = w_var.numpy()[0, 0]
b_bce_opt = b_var.numpy()[0]
print(w_bce_opt, b_bce_opt)
