#!/usr/bin/env python3

import numpy as np
from scipy.optimize import minimize

x = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
y = np.array([0, 0, 1, 0, 1, 1, 1, 1, 1, 1])


def sigmoid(x: np.ndarray) -> np.ndarray:
    """Compute the componentwise sigmoid function for a numpy array."""
    return 1 / (1 + np.exp(-x))


def logistic_regression_bce_loss(w_and_b: np.ndarray) -> float:
    """Compute the cumulative binary cross-entropy.

    Parameters
    ----------
    w_and_b : np.ndarray
        A numpy array of shape (2,) with components w (weight) and b (bias), suitable
        for use with scipy.optimize.minimize.

    Returns
    -------
    float
        The sum of BCE losses from the features x and measurements y.
    """
    w, b = w_and_b
    yhat = sigmoid(w * x + b)
    individual_losses = -(y * np.log(yhat) + (1 - y) * np.log(1 - yhat))
    cumulative_loss = individual_losses.sum()
    return cumulative_loss


w_init, b_init = 0, 0
"""Initial values when searching for a minimum."""

# Find and print the minima.
w_bce_opt, b_bce_opt = minimize(logistic_regression_bce_loss, (w_init, b_init)).x
print(w_bce_opt, b_bce_opt)
